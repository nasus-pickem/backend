FROM mcr.microsoft.com/dotnet/sdk:7.0.401-alpine3.18 as build-env
WORKDIR /src
RUN dotnet nuget add source --name nasus-pickem-api https://gitlab.com/api/v4/projects/50832404/packages/nuget/index.json
COPY src/Nasus.Backend/Nasus.Backend.csproj Nasus.Backend/Nasus.Backend.csproj
COPY src/Nasus.Backend.Migrations.PostgreSQL/Nasus.Backend.Migrations.PostgreSQL.csproj Nasus.Backend.Migrations.PostgreSQL/Nasus.Backend.Migrations.PostgreSQL.csproj
COPY src/Nasus.Backend.Migrations.Sqlite/Nasus.Backend.Migrations.Sqlite.csproj Nasus.Backend.Migrations.Sqlite/Nasus.Backend.Migrations.Sqlite.csproj
COPY src/Nasus.Backend.Models/Nasus.Backend.Models.csproj Nasus.Backend.Models/Nasus.Backend.Models.csproj
RUN dotnet restore Nasus.Backend
COPY src/ .
RUN dotnet publish Nasus.Backend -c Release -o /publish

FROM mcr.microsoft.com/dotnet/aspnet:7.0.11-alpine3.18 as runtime
WORKDIR /publish
COPY --from=build-env /publish .
EXPOSE 80
ENTRYPOINT ["dotnet", "Nasus.Backend.dll"]
