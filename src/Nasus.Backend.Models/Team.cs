namespace Nasus.Backend.Models;

public class Team
{
    public required long Id { get; set; }
    public required string Name { get; set; }
    public required string Logo { get; set; }
}
