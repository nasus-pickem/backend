using Microsoft.EntityFrameworkCore;

namespace Nasus.Backend.Models;

public class NasusContext
    : DbContext
{
    public NasusContext(DbContextOptions<NasusContext> options)
        : base(options)
    {
    }
    
    public DbSet<Team> Teams { get; set; }
    public DbSet<Match> Matches { get; set; }
}
