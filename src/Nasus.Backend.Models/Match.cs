namespace Nasus.Backend.Models;

public class Match
{
    public required long Id { get; set; }
    public required virtual Team Red { get; set; }
    public required virtual Team Blue { get; set; }
}
