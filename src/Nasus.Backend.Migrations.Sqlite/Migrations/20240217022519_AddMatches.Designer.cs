﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Nasus.Backend.Models;

#nullable disable

namespace Nasus.Backend.Migrations.Sqlite.Migrations
{
    [DbContext(typeof(NasusContext))]
    [Migration("20240217022519_AddMatches")]
    partial class AddMatches
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "7.0.11");

            modelBuilder.Entity("Nasus.Backend.Models.Match", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<long>("BlueId")
                        .HasColumnType("INTEGER");

                    b.Property<long>("RedId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("BlueId");

                    b.HasIndex("RedId");

                    b.ToTable("Matches");
                });

            modelBuilder.Entity("Nasus.Backend.Models.Team", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Logo")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("Nasus.Backend.Models.Match", b =>
                {
                    b.HasOne("Nasus.Backend.Models.Team", "Blue")
                        .WithMany()
                        .HasForeignKey("BlueId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Nasus.Backend.Models.Team", "Red")
                        .WithMany()
                        .HasForeignKey("RedId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Blue");

                    b.Navigation("Red");
                });
#pragma warning restore 612, 618
        }
    }
}
