﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Nasus.Backend.Models;

#nullable disable

namespace Nasus.Backend.Migrations.Sqlite.Migrations
{
    [DbContext(typeof(NasusContext))]
    [Migration("20240217021018_InitialCreate")]
    partial class InitialCreate
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "7.0.11");
#pragma warning restore 612, 618
        }
    }
}
