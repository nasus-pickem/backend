using Api = Nasus.Api.Aspnetcore.Models;
using Db = Nasus.Backend.Models;

namespace Nasus.Backend.Converters;

public class MatchConverter
{
    public static Api::Match Convert(Db::Match match)
    {
        return new Api::Match
        {
            Id = match.Id,
            Red = TeamConverter.Convert(match.Red),
            Blue = TeamConverter.Convert(match.Blue)
        };
    }
    public static Db::Match Convert(Api::Match match)
    {
        return new Db::Match
        {
            Id = match.Id,
            Red = TeamConverter.Convert(match.Red),
            Blue = TeamConverter.Convert(match.Blue)
        };
    }
}
