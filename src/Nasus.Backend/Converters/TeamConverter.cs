using Api = Nasus.Api.Aspnetcore.Models;
using Db = Nasus.Backend.Models;

namespace Nasus.Backend.Converters;

public class TeamConverter
{
    public static Api::Team Convert(Db::Team team)
    {
        return new Api::Team
        {
            Id = team.Id,
            Name = team.Name,
            Logo = team.Logo
        };
    }
    public static Db::Team Convert(Api::Team team)
    {
        return new Db::Team
        {
            Id = team.Id,
            Name = team.Name,
            Logo = team.Logo
        };
    }
}
