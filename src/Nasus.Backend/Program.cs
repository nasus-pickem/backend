using Microsoft.EntityFrameworkCore;
using Nasus.Backend;
using Nasus.Backend.Authentication;
using Nasus.Backend.Models;

var DevelopmentOrigins = "_NasusDevelopmentOrigins";
var StagingOrigins = "_NasusStagingOrigins";
var ProductionOrigins = "_NasusProductionOrigins";

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables(options => {
    options.Prefix = "NASUS_";
});

builder.Services.AddDbContext<NasusContext>(options =>
{
    var provider = builder.Configuration.GetValue("provider", Provider.PostgreSQL.Name);
    if (provider == Provider.Sqlite.Name)
    {
        options.UseSqlite(
            builder.Configuration.GetConnectionString(Provider.Sqlite.Name)!,
            x => x.MigrationsAssembly(Provider.Sqlite.Assembly)
        );
    }
    if (provider == Provider.PostgreSQL.Name) {
        options.UseNpgsql(
            builder.Configuration.GetConnectionString(Provider.PostgreSQL.Name)!,
            x => x.MigrationsAssembly(Provider.PostgreSQL.Assembly)
        );
    }
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpClient();

builder.Services.AddCors(options =>
{
    options.AddPolicy(
        DevelopmentOrigins,
        policy =>
        {
            policy.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
    options.AddPolicy(
        StagingOrigins,
        policy =>
        {
            policy.WithOrigins("https://*.nasus-pickem-frontend.pixxon.hu")
                .SetIsOriginAllowedToAllowWildcardSubdomains();
        });
    options.AddPolicy(
        ProductionOrigins,
        policy =>
        {
            policy.WithOrigins("https://nasus-pickem-frontend.pixxon.hu");
        });
});

builder.Services.AddAuthentication(DiscordDefaults.AuthenticationScheme)
    .AddDiscord();

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Admin", policy => policy.RequireUserName("pixxon"));
    options.AddPolicy("User", policy => policy.RequireAuthenticatedUser());
});

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var service = scope.ServiceProvider;
    var context = service.GetService<NasusContext>();
    context!.Database.EnsureCreated();
}

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors(DevelopmentOrigins);
}

if (app.Environment.IsStaging())
{
    app.UseCors(StagingOrigins);
}

if (app.Environment.IsProduction())
{
    app.UseCors(ProductionOrigins);
}

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
