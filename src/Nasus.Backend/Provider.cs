namespace Nasus.Backend;

public record Provider(string Name, string Assembly) 
{
    public static Provider Sqlite = new (nameof(Sqlite), typeof(Migrations.Sqlite.Marker).Assembly.GetName().Name!);
    public static Provider PostgreSQL = new (nameof(PostgreSQL), typeof(Migrations.PostgreSQL.Marker).Assembly.GetName().Name!);
}
