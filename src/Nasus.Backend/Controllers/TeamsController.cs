using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nasus.Api.Aspnetcore.Controllers;
using Nasus.Backend.Converters;
using Nasus.Backend.Models;

using Api = Nasus.Api.Aspnetcore.Models;

namespace Nasus.Backend.Controllers;

public class TeamsController
    : TeamsApiController
{
    private readonly NasusContext _context;

    private readonly ILogger<TeamsController> _logger;

    public TeamsController(ILogger<TeamsController> logger, NasusContext context)
    {
        _logger = logger;
        _context = context;
    }
    
    public override IActionResult GetTeam(long teamId)
    {
        var team = _context.Teams.FirstOrDefault(t => t.Id == teamId);
        if (team == null)
        {
            return NotFound();
        }
        return Ok(TeamConverter.Convert(team));
    }

    public override IActionResult GetTeams()
    {
        return Ok(_context.Teams);
    }

    public override IActionResult SetTeam(Api::Team team)
    {
        if (_context.Teams.Any(t => t.Id == team.Id))
        {
            return Forbid();
        }
        _context.Teams.Add(TeamConverter.Convert(team));
        _context.SaveChanges();
        return CreatedAtAction(nameof(GetTeam), new { teamId = team.Id }, team);
    }
}
