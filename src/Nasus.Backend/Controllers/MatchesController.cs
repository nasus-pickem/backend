using Microsoft.AspNetCore.Mvc;
using Nasus.Api.Aspnetcore.Controllers;
using Nasus.Backend.Converters;
using Nasus.Backend.Models;

using Api = Nasus.Api.Aspnetcore.Models;

namespace Nasus.Backend.Controllers;

public class MatchesController
    : MatchesApiController
{
    private readonly NasusContext _context;

    private readonly ILogger<MatchesController> _logger;

    public MatchesController(ILogger<MatchesController> logger, NasusContext context)
    {
        _logger = logger;
        _context = context;
    }
    
    public override IActionResult GetMatch(long matchId)
    {
        var team = _context.Matches.First(t => t.Id == matchId);
        if (team == null)
        {
            return NotFound();
        }
        return Ok(MatchConverter.Convert(team));
    }

    public override IActionResult GetMatches()
    {
        return Ok(_context.Matches);
    }

    public override IActionResult SetMatch(Api::Match match)
    {
        if (_context.Matches.Any(t => t.Id == match.Id))
        {
            return Forbid();
        }
        if (!_context.Teams.Any(t => t.Id == match.Red.Id))
        {
            return Forbid();
        }
        if (!_context.Teams.Any(t => t.Id == match.Blue.Id))
        {
            return Forbid();
        }
        _context.Matches.Update(MatchConverter.Convert(match));
        _context.SaveChanges();
        return CreatedAtAction(nameof(GetMatch), new { matchId = match.Id }, match);
    }
}
