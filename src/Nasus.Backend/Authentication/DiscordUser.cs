
using System.Text.Json.Serialization;

namespace Nasus.Backend.Authentication;

public class DiscordUser
{
    [JsonPropertyName("id")]
    public required string ID { get; set; }
    
    [JsonPropertyName("username")]
    public required string Username { get; set; }

    [JsonPropertyName("email")]
    public required string Email { get; set; }
}
