
using Microsoft.AspNetCore.Authentication;

namespace Nasus.Backend.Authentication;

public class DiscordOptions
    : AuthenticationSchemeOptions
{    
    public readonly string UserInformationEndpoint = DiscordDefaults.UserInformationEndpoint;
}
