
namespace Nasus.Backend.Authentication;

public static class DiscordDefaults
{
    public const string AuthenticationScheme = "Discord";
    
    public const string DisplayName = "Discord";
        
    public const string UserInformationEndpoint = "https://discordapp.com/api/users/@me";
}
