using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Text.Json;

namespace Nasus.Backend.Authentication;

public class DiscordHandler
    : AuthenticationHandler<DiscordOptions>
{
    private readonly IHttpClientFactory _httpClientFactory;

    public DiscordHandler(IHttpClientFactory httpClientFactory, IOptionsMonitor<DiscordOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
        : base(options, logger, encoder, clock)
    {
        _httpClientFactory = httpClientFactory;
    }

    protected async override Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        if (Request.Headers.TryGetValue("Authorization", out var headerAuth))
        {
            var token = headerAuth.First()!.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];

            var request = new HttpRequestMessage(HttpMethod.Get, Options.UserInformationEndpoint);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var client = _httpClientFactory.CreateClient();
            var response = await client.SendAsync(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var json = await response.Content.ReadAsStringAsync();
                var discordUser = JsonSerializer.Deserialize<DiscordUser>(json);
                var claims = new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, discordUser!.ID),
                    new Claim(ClaimTypes.Name, discordUser!.Username),
                    new Claim(ClaimTypes.Email, discordUser!.Email)
                };
                var identity = new ClaimsIdentity(claims, Scheme.Name);
                var principal = new ClaimsPrincipal(identity);
                var ticket = new AuthenticationTicket(principal, Scheme.Name);

                return AuthenticateResult.Success(ticket);
            }
            return AuthenticateResult.Fail("Failed authentication request");
        }
        return AuthenticateResult.Fail("No authorization header");
    }
}