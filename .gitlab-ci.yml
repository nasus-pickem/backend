prepare_version:
  stage: .pre
  image:
    name: psanetra/git-semver
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: []
  script:
    - git log
    - echo "TAG=$(git-semver next)" | tee -a tag.env
  artifacts:
    reports:
      dotenv: tag.env

prepare_changelog:
  stage: .pre
  image:
    name: node:20-alpine3.18
    entrypoint: [""]
  rules: 
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: []
  before_script:
    - apk add git
    - npm install --global conventional-changelog-cli 
  script:
    - conventional-changelog -p angular | tail -n +2 | tee -a _CHANGELOG.md
  artifacts:
    paths:
      - _CHANGELOG.md

create_release:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_version
      artifacts: true
    - job: prepare_changelog
      artifacts: true
  script:
    - echo "Creating new release $TAG"
  release:
    name: 'Release $TAG'
    description: './_CHANGELOG.md'
    tag_name: '$TAG'
    ref: '$CI_COMMIT_SHA'

verify_commits:
  stage: .pre
  image:
    name: node:20-alpine3.18
    entrypoint: [""]
  rules: 
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  needs: []
  before_script:
    - apk add git
    - npm install --global @commitlint/config-angular @commitlint/cli
  script:
    - commitlint --from $CI_MERGE_REQUEST_DIFF_BASE_SHA --extends @commitlint/config-angular --strict --verbose

build_image:
  stage: build
  image: docker:23.0.1
  services:
    - docker:23.0.1-dind
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: []
  variables:
    IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  script:
    - docker build -t $IMAGE .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker push $IMAGE

build_tag:
  stage: build
  image: docker:23.0.1
  services:
    - docker:23.0.1-dind
  rules:
    - if: $CI_COMMIT_TAG
  needs: []
  variables:
    CACHE_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  script:
    - docker pull $CACHE_IMAGE || true
    - docker build -t $IMAGE .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker push $IMAGE

.docker_cli_setup:
  image: alpine:3.17.2
  before_script:
    - apk add openssh-client gettext docker-cli curl
    - eval $(ssh-agent -s)
    - mkdir -p ~/.ssh && touch ~/.ssh/known_hosts && ssh-keyscan $DOCKER_HOSTNAME >> ~/.ssh/known_hosts
    - echo "$SSH_PRIVATE_KEY" | base64 -di | tr -d '\r' | ssh-add -
    - curl -fSL "https://github.com/genuinetools/reg/releases/download/v0.16.1/reg-linux-amd64" -o "/usr/local/bin/reg" && echo "ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228  /usr/local/bin/reg" | sha256sum -c - && chmod a+x "/usr/local/bin/reg"

deploy_review:
  stage: deploy
  extends: .docker_cli_setup
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  needs:
    - job: build_image
  variables:
    REVIEW_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    REVIEW_HOSTNAME: review-$CI_MERGE_REQUEST_IID.$CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME.$DOCKER_HOSTNAME
    REVIEW_STACKNAME: $CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-review-$CI_MERGE_REQUEST_IID
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - cat docker-compose.review.yaml | envsubst | docker stack deploy --compose-file - $REVIEW_STACKNAME --prune
  environment:
    name: review/$CI_MERGE_REQUEST_IID
    url: https://$REVIEW_HOSTNAME
    on_stop: stop_review

stop_review:
  stage: .post
  extends: .docker_cli_setup
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
      allow_failure: true
  needs:
    - job: deploy_review
  variables:
    REVIEW_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    REVIEW_STACKNAME: $CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-review-$CI_MERGE_REQUEST_IID
  script:
    - docker stack rm $REVIEW_STACKNAME
    - reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $REVIEW_IMAGE
  environment:
    name: review/$CI_MERGE_REQUEST_IID
    action: stop

deploy_staging:
  stage: deploy
  extends: .docker_cli_setup
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs:
    - job: build_image
  variables:
    STAGING_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    STAGING_HOSTNAME: staging.$CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME.$DOCKER_HOSTNAME
    STAGING_STACKNAME: $CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-staging
    STAGING_POSTGRESQL_USER: postgres
    STAGING_POSTGRESQL_PASSWORD: $POSTGRESQL_PASSWORD
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - cat docker-compose.staging.yaml | envsubst | docker stack deploy --compose-file - $STAGING_STACKNAME --prune
  environment:
    name: staging
    url: https://$STAGING_HOSTNAME
    on_stop: stop_staging

stop_staging:
  stage: .post
  extends: .docker_cli_setup
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
      allow_failure: true
  needs:
    - job: deploy_staging
  variables:
    STAGING_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    STAGING_STACKNAME: $CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-staging
  script:
    - docker stack rm $STAGING_STACKNAME
    - reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $STAGING_IMAGE
  environment:
    name: staging
    action: stop

deploy_production:
  stage: deploy
  extends: .docker_cli_setup
  rules:
    - if: $CI_COMMIT_TAG
  needs:
    - job: build_tag
  variables:
    PRODUCTION_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    PRODUCTION_HOSTNAME: $CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME.$DOCKER_HOSTNAME
    PRODUCTION_STACKNAME: $CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-production
    PRODUCTION_POSTGRESQL_USER: postgres
    PRODUCTION_POSTGRESQL_PASSWORD: $POSTGRESQL_PASSWORD
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - cat docker-compose.production.yaml | envsubst | docker stack deploy --compose-file - $PRODUCTION_STACKNAME --prune
  environment:
    name: production
    url: https://$PRODUCTION_HOSTNAME
    on_stop: stop_production

stop_production:
  stage: .post
  extends: .docker_cli_setup
  rules:
    - if: $CI_COMMIT_TAG
      when: manual
      allow_failure: true
  needs:
    - job: deploy_production
  variables:
    PRODUCTION_STACKNAME: $CI_PROJECT_ROOT_NAMESPACE-$CI_PROJECT_NAME-production
  script:
    - docker stack rm $PRODUCTION_STACKNAME
  environment:
    name: production
    action: stop
